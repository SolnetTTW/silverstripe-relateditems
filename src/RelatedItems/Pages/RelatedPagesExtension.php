<?php

namespace Solnet\RelatedItems\Pages;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Lumberjack\Forms\GridFieldConfig_Lumberjack;
use SilverStripe\Lumberjack\Forms\GridFieldSiteTreeAddNewButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * Extension to enable a DataObject subclass to have a relation to a set of related SiteTree
 * objects, and add the CMS interface for managing these.
 */
class RelatedPagesExtension extends DataExtension
{
    use Configurable;

    /**
     * @config
     * Sets the default maximum number of related pages we can add for each item.
     */
    private static $default_max_related_pages = 10;

    private static $has_many = [
        'RelatedPageObjects' => RelatedPage::class.'.Parent',
    ];

    private static $owns = [
        'RelatedPageObjects',
    ];

    private static $cascade_deletes = [
        'RelatedPageObjects',
    ];

    private static $cascade_duplicates = [
        'RelatedPageObjects',
    ];

    /**
     * Returns the list of SiteTree objects this dataobject is related to.
     *
     * @return DataList
     */
    public function getRelatedPages()
    {
        return SiteTree::get()
        ->innerJoin('RelatedPage', '"RelatedPage"."RelatedID" = "SiteTree"."ID"')
        ->where('"RelatedPage"."ParentID" = '.$this->owner->ID);
    }

    public function updateCMSFields(FieldList $fields)
    {
        if ($this->owner->exists()) {
            $fields->addFieldsToTab(
                'Root.Related',
                [
                    GridField::create(
                        'RelatedPageObjects',
                        _t('RelatedItems.RelatedPages_RelatedPageObjects_Title', 'Related Pages'),
                        $this->owner->RelatedPageObjects(),
                        $config = GridFieldConfig_RelationEditor::create()
                        ->addComponent(
                            new GridFieldOrderableRows()
                        )
                    )
                ]
            );
            // Set maximum related pages
            $max = $this->config()->default_max_related_pages;
            if ($this->owner->config()->max_related_pages) {
                $max = $this->owner->config()->max_related_pages;
            }
            // Remove AddExistingAutocompleter - these are relation objects
            $config->removeComponentsByType(GridFieldAddExistingAutocompleter::class);
            // Remove Add button if we have max already.
            // Doesn't enforce, but does remove the UI that enables it.
            if ($this->owner->RelatedPageObjects()->Count() >= $max) {
                $config->removeComponentsByType(GridFieldAddNewButton::class);
            }
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('RelatedItems.RelatedPages_SavingTip', '<p class="message warning">Please save to edit related pages.</p>')
                )
            );
        }
    }
}
