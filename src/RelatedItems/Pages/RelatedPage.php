<?php

namespace Solnet\RelatedItems\Pages;

use SilverStripe\ORM\DataObject;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\Versioned\Versioned;

/**
 * Maps SiteTree objects to a parent DataObject.
 *
 * Used both as a polymorphic join table:
 * https://docs.silverstripe.org/en/4/developer_guides/model/relations/#polymorphic-many-many-experimental
 *
 * And also as a CMS editing interface allowing picking SiteTree via tree picker.
 */


class RelatedPage extends DataObject
{
    private static $table_name = 'RelatedPage';
    private static $singular_name = 'Related Page';
    private static $plural_name = 'Related Pages';

    private static $db = [
        'Sort' => 'Int',
    ];

    private static $has_one = [
        'Parent' => DataObject::class,
        'Related' => SiteTree::class,
    ];

    private static $summary_fields = [
        'Related.Title' => 'Title',
        'Related.Link' => 'Link',
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Sort');
        $fields->removeByName('LinkTracking');
        $fields->removeByName('FileTracking');
        $fields->replaceField(
            'RelatedID',
            TreeDropdownField::create(
                'RelatedID',
                _t('RelatedItems.RelatedPage_Related_Title', 'Related page'),
                SiteTree::class
            )
        );

        return $fields;
    }
}
