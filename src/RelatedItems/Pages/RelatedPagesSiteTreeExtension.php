<?php

namespace Solnet\RelatedItems\Pages;

use SilverStripe\ORM\DataExtension;

/**
 * Link table pointing SiteTree objects back at their parent DataObjects
 * via the RelatedPage link table.
 */

class RelatedPagesSiteTreeExtension extends DataExtension
{
    private static $has_many = [
        'RelatingItems' => RelatedPage::class.'.Related',
    ];
}
