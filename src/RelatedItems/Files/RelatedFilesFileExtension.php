<?php

namespace Solnet\RelatedItems\Files;

use SilverStripe\ORM\DataExtension;

/**
 * Link table pointing File objects back at their parent DataObjects
 * via the RealtedFile link table.
 */

class RelatedFilesFileExtension extends DataExtension
{
    private static $has_many = [
        'RelatingItems' => RelatedFile::class,
    ];
}
