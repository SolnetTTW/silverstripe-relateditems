<?php

namespace Solnet\RelatedItems\Files;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Assets\File;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\LiteralField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * Extension to enable a DataObject subclass to have a relation to a set of related File
 * objects, and add the CMS interface for managing these.
 */
class RelatedFilesExtension extends DataExtension
{
    use Configurable;

    /**
     * @config
     * Sets the default maximum number of related files we can add for each item.
     */
    private static $default_max_related_files = 10;

    private static $has_many = [
        'RelatedFileObjects' => RelatedFile::class.'.Parent',
    ];

    private static $owns = [
        'RelatedFileObjects',
    ];

    private static $cascade_deletes = [
        'RelatedFileObjects',
    ];

    private static $cascade_duplicates = [
        'RelatedFileObjects',
    ];

    /**
     * Returns the list of SiteTree objects this dataobject is related to.
     *
     * @return DataList
     */
    public function getRelatedFiles()
    {
        return File::get()
        ->innerJoin('RelatedFile', '"RelatedFile"."RelatedID" = "File"."ID"')
        ->where('"RelatedFile"."ParentID" = '.$this->owner->ID);
    }

    public function updateCMSFields(FieldList $fields)
    {
        if ($this->owner->exists()) {
            $fields->addFieldsToTab(
                'Root.Related',
                [
                    GridField::create(
                        'RelatedFileObjects',
                        _t('RelatedItems.RelatedFiles_RelatedFileObjects_Title', 'Related Files'),
                        $this->owner->RelatedFileObjects(),
                        $config = GridFieldConfig_RelationEditor::create()
                        ->addComponent(
                            new GridFieldOrderableRows()
                        )
                    )
                ]
            );
            // Remove AddExistingAutocompleter - these are relation objects
            $config->removeComponentsByType(GridFieldAddExistingAutocompleter::class);
            // Set maximum related files
            $max = $this->config()->default_max_related_files;
            if ($this->owner->config()->max_related_files) {
                $max = $this->owner->config()->max_related_files;
            }
            // Remove Add button if we have max already.
            // Doesn't enforce, but does remove the UI that enables it.
            if ($this->owner->RelatedFileObjects()->Count() >= $max) {
                $config->removeComponentsByType(GridFieldAddNewButton::class);
            }
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('RelatedItems.RelatedFiles_SavingTip', '<p class="message warning">Please save to edit related files.</p>')
                )
            );
        }
    }
}
