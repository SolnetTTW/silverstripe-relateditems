<?php

namespace Solnet\RelatedItems\Files;

use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;

/**
 * Maps File objects to a parent DataObject.
 *
 * Used both as a polymorphic join table:
 * https://docs.silverstripe.org/en/4/developer_guides/model/relations/#polymorphic-many-many-experimental
 *
 * And also as a CMS editing interface allowing picking Files via upload field.
 */


class RelatedFile extends DataObject
{
    private static $table_name = 'RelatedFile';
    private static $singular_name = 'Related File';
    private static $plural_name = 'Related Files';

    private static $db = [
        'Sort' => 'Int',
    ];

    private static $has_one = [
        'Parent' => DataObject::class,
        'Related' => File::class,
    ];

    private static $summary_fields = [
        'Related.Title' => 'Title',
        'Related.Link' => 'Link',
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Sort');
        $fields->replaceField(
            'RelatedID',
            UploadField::create(
                'Related',
                _t('RelatedItems.RelatedFile_Related_Title', 'Related file')
            )
        );

        return $fields;
    }
}
